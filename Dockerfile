FROM alpine:latest
RUN sed -i "s/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g" /etc/apk/repositories
RUN apk add build-base openmpi openmpi-dev
COPY hello hello
RUN rm -fr hello/*~
RUN ls
#RUN ls hello
#RUN cd hello
WORKDIR hello
RUN mpicc hello.c -o hello
